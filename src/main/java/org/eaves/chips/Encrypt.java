package org.eaves.chips;

public interface Encrypt
{
    byte[] encrypt(String passphrase, Conversations input);

    byte[] decrypt(String key, byte[] cipher);
}
