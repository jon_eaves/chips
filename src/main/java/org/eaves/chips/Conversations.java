package org.eaves.chips;

import java.util.List;

public interface Conversations
{
    Conversation newConversation();

    Conversation openConversation(String thread);
    
    List<String> list();

    void rename(String oldName, String newName);
    
    byte[] asBytes();
    
    Conversations fromBytes(byte[] input);

    Conversation first();
}
