package org.eaves.chips;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryConversations implements Conversations, Serializable
{
    private Map<String, Conversation> _conversations = new HashMap();
    private int _counter = 0;

    @Override
    public Conversation newConversation()
    {
        String name = createName();
        while (_conversations.containsKey(name))
        {
            name = createName();
        }
        Conversation rv = new Conversation(name);
        _conversations.put(name, rv);
        return rv;
    }

    private String createName()
    {
        return "NC" + _counter++;
    }

    @Override
    public Conversation openConversation(String thread)
    {
        Conversation rv;
        if (_conversations.containsKey(thread))
        {
            rv = _conversations.get(thread);
        } else
        {
            rv = newConversation();
        }
        return rv;
    }

    @Override
    public List<String> list()
    {
        return new ArrayList(_conversations.keySet());
    }

    @Override
    public void rename(String oldName, String newName)
    {
        Conversation old;
        if (_conversations.containsKey(oldName))
        {
            old = _conversations.get(oldName);
            _conversations.put(newName, old.rename(newName));
            _conversations.remove(oldName);
        }
    }

    @Override
    public byte[] asBytes()
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try
        {
            ObjectOutput oo = new ObjectOutputStream(bos);
            oo.writeObject(this);
            oo.flush();
            bos.flush();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return bos.toByteArray();
    }
    
    public Conversations fromBytes(byte[] input)
    {
        Conversations rv = null;
        ByteArrayInputStream bis = new ByteArrayInputStream(input);
        try
        {
            ObjectInput oi = new ObjectInputStream(bis);
            rv = (Conversations) oi.readObject();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();;
        }
        return rv;
    }

    @Override
    public Conversation first()
    {
        return _conversations.get(_conversations.keySet().toArray()[0]);
    }

}
