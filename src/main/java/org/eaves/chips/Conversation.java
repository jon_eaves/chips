package org.eaves.chips;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Conversation implements Serializable
{
    private final List<String> _lines;
    private final String _title;
    private boolean _open;

    public Conversation(String title)
    {
        _title = title;
        _lines = new ArrayList<String>();
    }

    private Conversation(Conversation copy, String newName) {
        this._title = newName;
        this._lines = new ArrayList<>(copy._lines);
    }

    public void comment(String author, String line)
    {
        _lines.add(format(author, line));
    }

    public String title() {
        return _title;
    }

    public String out()
    {
        return String.join("\n", _lines);
    }

    private String format(String author, String line)
    {
        return author + " : " + line;
    }
    
    public Conversation rename(String newName)
    {
        return new Conversation(this, newName);
    }

}
