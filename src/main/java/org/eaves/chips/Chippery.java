package org.eaves.chips;

import asg.cliche.Command;
import asg.cliche.ShellFactory;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import org.bouncycastle.util.encoders.Base64;

import java.io.IOException;

public class Chippery
{

    @Parameter(names = {"--local", "-l"})
    private static boolean _local = true;

    private Conversations _threads;
    private final Encrypt _encrypt;
    private Conversation _current = null;
    private String _name;

    private Chippery(Conversations ds, Encrypt e)
    {
        _threads = ds;
        _name = "anon";
        _encrypt = e;
        
        newConversation();
    }

    public static void main(String... args) throws IOException
    {
        Chippery c = new Chippery(new InMemoryConversations(), new PBEncrypt());
        JCommander.newBuilder().addObject(c).build().parse(args);

        ShellFactory.createConsoleShell(":: ", "chippery", c).commandLoop();
    }

    @Command
    public void quit()
    {
        System.exit(0);
    }

    @Command(name = "new")
    public void newConversation()
    {
        Conversation rv =  _threads.newConversation();
        System.out.println("Starting a new conversation > " + rv.title() );
        _current = rv;
    }

    @Command(name = "open")
    public void openConversation(String thread)
    {
        _current = _threads.openConversation(thread);
        System.out.println("Opening a conversation > " + _current.title() );
    }

    @Command(name = "c")
    public void comment(String words)
    {
        _current.comment(_name, words);
    }
    
    @Command
    public void me(String name)
    {
        _name = name;
    }
    
    @Command 
    public String save(String key) 
    {
        byte[] cipher = _encrypt.encrypt(key, _threads);
        return Base64.toBase64String(cipher);
    }
    
    @Command
    public void load(String key, String input) 
    {
        byte[] cipher = Base64.decode(input);
        byte[] raw = _encrypt.decrypt(key, cipher);
        
        Conversations loaded = _threads.fromBytes(raw);
        if (loaded != null) {
            _threads = loaded;
        }
        list();
        _current = _threads.first();
    }

    @Command
    public String dump()
    {
        return _current.out();
    }
    
    @Command
    public String list()
    {
        return String.join(",", _threads.list());
    }
    
    @Command
    public void rename(String oldName, String newName)
    {
        boolean relink = false;
        if (_current.title().equals(oldName)) {
            relink = true;
        }
        _threads.rename(oldName, newName);
        if (relink) {
            openConversation(newName);
        }
    }
}
