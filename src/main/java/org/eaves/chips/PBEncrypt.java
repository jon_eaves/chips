package org.eaves.chips;

import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;

import java.security.SecureRandom;


public class PBEncrypt implements Encrypt
{
    private BufferedBlockCipher	_cipher = null;
    
    public PBEncrypt() {
        _cipher = new PaddedBufferedBlockCipher(
                new CBCBlockCipher(new AESEngine()));
    }
    
    @Override
    public byte[] encrypt(String passphrase, Conversations input)
    {
        PBEParametersGenerator g = new PKCS5S2ParametersGenerator();

        g.init(PBEParametersGenerator.PKCS5PasswordToBytes(passphrase.toCharArray()),
                null,
                32);

        CipherParameters cp = g.generateDerivedParameters( 256 );
        
        _cipher.init(true, cp);
        
        byte[] bytes = input.asBytes();
        int full = _cipher.getOutputSize(bytes.length);
        byte[] rv = new byte[full];
        
        int len = _cipher.processBytes(bytes, 0, bytes.length, rv, 0);
        try
        {
            _cipher.doFinal(rv, len);
        } catch (InvalidCipherTextException e)
        {
            e.printStackTrace();
        }
        return rv;
    }

    @Override
    public byte[] decrypt(String passphrase, byte[] cipher)
    {
        PBEParametersGenerator g = new PKCS5S2ParametersGenerator();

        g.init(PBEParametersGenerator.PKCS5PasswordToBytes(passphrase.toCharArray()),
                null,
                32);

        CipherParameters cp = g.generateDerivedParameters( 256 );
        
        _cipher.init(false, cp);
        
        byte[] grp = new byte[_cipher.getOutputSize(cipher.length)];
        int len = _cipher.processBytes(cipher, 0, cipher.length, grp, 0);
        
        try {
            len += _cipher.doFinal(grp, len);
        } catch (InvalidCipherTextException e)
        {
            e.printStackTrace();
        }
        
        return grp;


    }
}
