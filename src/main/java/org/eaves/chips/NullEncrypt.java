package org.eaves.chips;

public class NullEncrypt implements Encrypt
{
    @Override
    public byte[] encrypt(String passphrase, Conversations input)
    {
        return input.asBytes();
    }

    @Override
    public byte[] decrypt(String key, byte[] cipher)
    {
        return cipher;
    }
}
