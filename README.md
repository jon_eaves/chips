# chips

Chat in prolonged secrecy.  Or the sorts of things you do to a potato.

This is a very simple chat / messaging application with a multi-protocol engine options for transfer of the content.  

It's been written as a proof of concept to show how ridiculous the Australian Governments "war on math" really is, because anybody with about 4 hours to spare can write something like this from scratch.  I've not actually done anything to store the output, but you could cut and paste it into IRC, or a gist.

It uses PBE - and doesn't do anything with known plaintext (I could include random numbers and hashes for checking the consistency of the data).  The code can trivially be extended to use asymmetric crypto to share keys, or other schemes for greater security.

Don't be fooled, anybody who wants to keep their communications secrect can do exactly this.  The only thing that the stupid laws is going to do is make it easier for bad people to break any publicly available encrypted traffic.  So, back doors in WhatsApp just makes it bad for the public - and has no effect on people who actually want to encrypt and keep the information secure.

# Usage

The application uses the cliche-shell.  So, after compiling and running the application the options are


* me <name> - sets the username to <name>
* c - add a comment to the current thread
* new - creates a new thread
* open <thread> - opens an existing thread called <thread>
* dump - displays the current thread
* list - shows all the available threads

* save <password> - saves all threads, encrypts and returns a string
* load <password> <string> - loads all the threads from <string> encrypted with <password>

If you screw up the password, or generally make a mistake using the application, then lots of exceptions are thrown. 
I have invested no time in caring about making the application "friendly" for non-me consumption.





